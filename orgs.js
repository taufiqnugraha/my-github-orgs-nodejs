let https = require('https')
let get = (orgs_name) => {
    const options ={
    hostname: 'api.github.com',
    port: 443,
    path: `/orgs/${orgs_name}`,
    method: 'GET',
    headers:{
        'user-agent': 'nodejs'
    }
    //https://api.github.com/orgs/
    } 
    let request = https.request(options, (response)=>{
    let body = '';
    response.on('data', (data) =>{
        body = body + data;
        
    })
    response.on('end', ()=>{
        let orgs = JSON.parse(body);
        if(response.statusCode === 200){
            console.log(`nama ${orgs.login} public repo ${orgs.public_repos} location ${orgs.location} blog ${orgs.blog}`);
        }
        else{
            console.log(`username ${options.path} not found`)
        }
    })
    //parse

    })

    request.end()
    request.on('error', (e) => {
    console.log(e);
    })
}
module.exports ={
    get
}
//different way but still work
//exports.get = get;